const express = require('express');
const employeeController = require('../controllers/employeeRouteController');

// intiaize router
var router = express.Router();


// Create Employee routes
router.get('/',(req,res)=>{
    res.send('Hello Employee is here.');
});

// Employee routes
router.post('/addEmployee',employeeController.addEmployee);
router.get('/:id',employeeController.getEmployee);
router.post('/department',employeeController.getDeptEmployees);
router.put('/update/:id',employeeController.updateEmployee);

// Export router as middleware
module.exports = router;