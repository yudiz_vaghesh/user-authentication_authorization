// import modules
const express = require('express');
const userRouteController = require('../controllers/usersRouteController');
const cookieParser = require('cookie-parser');
const { checkUser,userAuth } = require('../middleware/userAuthToken');
const {upload} = require('../middleware/gridfsConnection');

// user the express.Router() class
var router = express.Router();
router.use(cookieParser());

// created all routes below

// home page - GET request
router.get('*',checkUser);  // check user authentication
router.get('/main',(req,res)=>{
    res.render('main');
});
router.get('/gallary',userAuth,(req,res)=>{
    res.render('gallary');
});


// GET Requests
router.get('/signup',userRouteController.getSignup);
router.get('/login',userRouteController.getLogin);
router.get('/logout',userRouteController.getLogout);
router.get('/upload',userRouteController.getUploadFiles);

// POST Requests
router.post('/signup',userRouteController.postSignup);
router.post('/login',userRouteController.postLogin);
router.post('/upload',upload.single('file'),userRouteController.postUploadFiles);



// export all routes from here
module.exports = router;