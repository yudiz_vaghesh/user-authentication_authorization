// import modules
const express = require('express');
const bodyParser = require('body-parser');
const userRoutes = require('./routes/usersRoutes');
const employeeRoutes = require('./routes/emploeeRoutes');

// initialize app in express
var app = express();

// use the bodyParser for form or post data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extendedL:false}));

// set public folder for app
app.use(express.static('public'));

// set our view and view engine
app.set('views','./views');
app.set('view engine','ejs');

// user external files here
app.use('/users',userRoutes)
app.use('/employees',employeeRoutes)
require('./middleware/dbConnection'); // Database Connection

// run localhost server
app.listen(5000,()=>{
    console.log('Localhost started on 5000.');
})


