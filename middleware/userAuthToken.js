// import modules
const jwt = require('jsonwebtoken');
const middleware = {};
// user authorization for specific access
// User JWT Authentication
middleware.userAuth = (req, res, next)=>{

    const token = req.cookies.userAuth;
    
    // check that user is authenticated or not
    if(token){
        jwt.verify(token,'user auth',(err, decodedToken)=>{
            if(err){
                console.log(err);
                res.redirect('/users/login');
            }
            else{
                console.log(decodedToken);
                next();
            }
        });
    }
    else{
        res.redirect('/users/login');
    }
    // next();
}


// check user is logged in or not
middleware.checkUser = (req,res,next)=>{
    const token = req.cookies.userAuth;
    
    // check that user is authenticated or not
    if(token){
        jwt.verify(token,'user auth',(err, decodedToken)=>{
            if(err){
                console.log(err.message);
                res.locals.username = null;
                next();
            }
            else{
                console.log(decodedToken);
                res.locals.username = decodedToken.email;
                next();
            }
        });
    }
    else{
        res.locals.username = null;
        next();
    }
}

module.exports = middleware;