const userDB = require('../middleware/dbConnection')
const Grid = require('gridfs-stream');
const mongoose = require('mongoose');
const GridFsStorage = require('multer-gridfs-storage');
const multer = require('multer');
const crypto = require('crypto');
const path = require('path');

// GridFS Initialization
let gfs;  //
    

userDB.once('open',()=>{
    // Intialize the GridFS
    gfs = Grid(userDB.db , mongoose.mongo);
    gfs.collection('uploads');
});

// Create Storage engine'
const storage = new GridFsStorage({
    url: 'mongodb://localhost:27017/userDetails',
    options: { useNewUrlParser: true, useUnifiedTopology: true },
    file: (req, file) => {
        // Using promise
      return new Promise((resolve, reject) => {
        crypto.randomBytes(16, (err, buf) => {
          if (err) {
            return reject(err);
          }
          const filename = buf.toString('hex') + path.extname(file.originalname);
          const fileInfo = {
            filename: filename,
            userEmail: 'admin@gmail.com',
            bucketName: 'uploads'
          };
          resolve(fileInfo);
        });
      });
    }
});

// create multer instance to pass storage engine
const upload = multer({storage});


module.exports = {upload,gfs};
