var mongoose = require('mongoose');


const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
    autoIndex: false, // Don't build indexes
    poolSize: 10, // Maintain up to 10 socket connections
    serverSelectionTimeoutMS: 5000, // Keep trying to send operations for 5 seconds
    socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
    family: 4 // Use IPv4, skip trying IPv6
  };

//Set up default mongoose connection
const usersDB = ()=>{
    return  mongoose.createConnection('mongodb://localhost:27017/userDetails',options);
    // create storage engine
}

usersDB().then(()=>console.log('Connected to mongoDB userDetails...'))
       .catch((err)=>console.error('Could not connected to mongoDB',err));
       
       
const employeeDB = ()=>{
        return  mongoose.createConnection('mongodb://localhost:27017/employeeDetails',options);
        // create storage engine
    }
    
employeeDB().then(()=>console.log('Connected to mongoDB employeeDetails...'))
            .catch((err)=>console.error('Could not connected to mongoDB',err));
           

module.exports = usersDB();
module.exports = employeeDB();
       // module.exports = connectDB;
       // module.exports.usersDB = ()=>{
       //     mongoose.connect('mongodb://localhost:27017/userDetails',options)
       //             .then(()=>console.log('Connected to mongoDB...'))
       //             .catch((err)=>console.error('Could not connected to mongoDB',err));
       // } 