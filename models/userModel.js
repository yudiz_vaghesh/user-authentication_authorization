// import modules 
const mongoose = require('mongoose');
const userDB  = require('../middleware/dbConnection');


// create user schema here
const userSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true,
        minLength: 1
    },
    email:{
        type: String,
        required: true,
        unique: true
    },
    password:{
        type: String,
        required: true,
        minLength: 6
    },
    authToken: String
},{timestamps: true});

// create User model using mongoose
const User = userDB.model('user',userSchema);
// // const User = mongoose.model('user',userSchema);

// export User model
// module.exports = {//// User };
module.exports = User;
