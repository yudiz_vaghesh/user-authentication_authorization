// import modules
const mongoose = require('mongoose');
const employeeDB = require('../middleware/dbConnection');

// create employee schema
const employeeSchema = new mongoose.Schema({
    eName:{
        type: String,
        minLength: 1,
        required: true
    },
    eDepartment:{
        type: String,
        required: true
    },
    eEmail:{
        type: String,
        required: true,
        unique: true
    },
    eMobile:{
        type: Number,
        required: true
    },
    eDOB:{
        type: Date,
        reuired: true   
    }
},{timestamps:true});

// Create Employee Model in employeeDB
const Employee = employeeDB.model('employee',employeeSchema);

module.exports = Employee;
