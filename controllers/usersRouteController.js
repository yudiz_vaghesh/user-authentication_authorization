// In this file we just created route controllers
const mongoose = require('mongoose');
const  User  = require('../models/userModel');
const jwt = require('jsonwebtoken');
const {gfs} = require('../middleware/gridfsConnection');



// For Signup page - GET request
module.exports.getSignup = (req,res)=>{
    res.render('signup');
} 

// For Signup page - GET request
module.exports.getLogin = (req,res)=>{
    res.render('login');
} 


// For Add user in db using signup - Post request
module.exports.postSignup =  async (req,res)=>{
    
    let { sfullname,semail,spassword } = req.body;
    // var token = this.generateToken
    // pass users data
    let user = new User({
        name: sfullname,
        email: semail,
        password: spassword,
        authToken: null
    });

    // save user in db
    user = await user.save();
    res.send(user);
}

// For verifying user in login - Post request 
module.exports.postLogin = async (req,res)=>{
    let { lemail, lpassword } = req.body;

    let user = await User.findOne({email:lemail}); // get valid user from DB

    if(!user) return res.status(404).send('User is not registered.'); // if not get user show error

    // let token = res.cookie.userAuth;

    // if(token==user.authToken){
    //     res.send('Authentication Error');
    // }
    // else{
        token = this.generateToken(user._id,user.email); //generate token using user_id and email 
        // console.log(token);
        if(user.password == lpassword){
            user.authToken = await token;
            user = await user.save();
            res.cookie('userAuth',token,{ httpOnly:true, maxAge: 30 * 60 * 1000});
            res.redirect('/users/main');
        }
        else{
                res.send('incorrect Username or Password !')
        }    
    // }

}

// For logout - GET request
module.exports.getLogout = (req,res)=>{


    res.cookie('userAuth','',{maxAge:1});
    res.redirect('/users/main');
}


// For uploadFile GET request
module.exports.getUploadFiles = (req,res) =>{
    res.render('upload');
}

// for uploadFile POST request
module.exports.postUploadFiles = (req,res)=>{
    res.json(req.file.originalname);
}

// Create route for Getting files
module.exports.getFiles = (req,res)=>{
    gfs.files.find().toArray((err,files)=>{
        // check for files 
        if(!files || files.length===0){
            return res.status(404).send('not found');
        }
    });
}
// Generate jwt token for auth,
module.exports.generateToken = (id,email)=>{
    return jwt.sign({id,email},'user auth',{expiresIn: 30 * 60 * 1000});
}