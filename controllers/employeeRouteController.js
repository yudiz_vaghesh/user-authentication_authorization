
const Employee = require('../models/employeeModel');

const addEmployee = async (req,res)=>{
    try{     
        let employee = new Employee ({
            eName: req.body.name,
            eDepartment: req.body.department,
            eEmail: req.body.email,
            eMobile: req.body.mobile,
            eDOB : req.body.dob
        });
    
        employee = await employee.save();
        res.send(employee);
    }
    catch(err){console.log(err);}
}

const getEmployee = async (req,res)=>{

    let employee = await Employee.findOne({ _id:req.params.id});

    if(!employee) return res.status(404).send('User Not Found !!!');

    res.send(employee);
}

const getDeptEmployees = async (req,res)=>{
    
    let employees = await Employee.find({eDepartment: req.body.department}).limit(5);

    if(!employees) return res.status(500).send('Something went wrong!!!');

    res.send(employees);
}

const updateEmployee = async(req,res)=>{
    try{

        employee = await Employee.updateOne({_id:req.params.id},{
            $set:{
                eName: req.body.name,
                eDepartment: req.body.department,
                eEmail: req.body.email,
                eMobile: req.body.mobile,
                eDOB : req.body.dob
            }
        });
        res.send(employee);
    }
    catch(err){
        console.log(err);
    }
}
// export modules
module.exports = {
    addEmployee,
    getEmployee,
    getDeptEmployees,
    updateEmployee
};